import react from 'react';
import React, { Component, useState } from 'react';
import { View, Text, Image, StyleSheet, FlatList, TouchableHighlight, NativeAppEventEmitter } from 'react-native';
import {useNavigation} from '@react-navigation/native';


export default function BarracaList(){
	const navigation = useNavigation();
	
	
 	const [barraca, SetBarraca] = useState([
				{
					id:1,
					title:'Barraca do João',
					img:require('../assets/images/produtor/barracaJoao.png'),
					description:'Frutas, Legumes e verduras.',
					bg:'#e35339',
					products:[
						{id:1, name:'Alface', img:require('../assets/images/produto/joao/alface.png')},
						{id:2, name:'Banana', img:require('../assets/images/produto/joao/banana.png')},
						{id:3, name:'Cebolinha', img:require('../assets/images/produto/joao/cebolinha.png')},
						{id:3, name:'Cenoura', img:require('../assets/images/produto/joao/cenoura.png')}
					]
				},
				{
					id:2,
					title:'Barraca do José',
					img:require('../assets/images/produtor/barracaJose.png'),
					description:'Pratos saudáveis para você!',
					bg:'#a6bb24',
					products:[
						{id:1, name:'Jaca', img:require('../assets/images/produto/jose/jaca.png')},
						{id:2, name:'Limao', img:require('../assets/images/produto/jose/limao.png')},
						{id:3, name:'Mandioca', img:require('../assets/images/produto/jose/mandioca.png')}
					]
				},
				{
					id:3,
					title:'Barraca da Maria',
					img:require('../assets/images/produtor/barracaMaria.png'),
					description:'Produtos Frescos com entrega grátis!',
					bg:'#079ed4',
					products:[
						{id:1, name:'Doce de Banana', img:require('../assets/images/produto/maria/doceDeBanana.png')},
						{id:2, name:'Doce de Leite', img:require('../assets/images/produto/maria/doceDeLeite.png')},
						{id:3, name:'Queijo Fresco', img:require('../assets/images/produto/maria/queijo.png')}
					]
				},
				{
					id:4,
					title:'Barraca do Silva',
					img:require('../assets/images/produtor/barraca.png'),
					description:'Melhores ofertas da região!',
					bg:'#fcb81c',
					products:[
						{id:1, name:'Queijo de Cabra', img:require('../assets/images/produto/silva/farinha.png')},
						{id:2, name:'Fécula de Mandioca', img:require('../assets/images/produto/silva/fecula.png')},
						{id:3, name:'Mandioca', img:require('../assets/images/produto/silva/mandioca.png')}
					]
				},
			]);
			return (
				<View style={styles.container}>
					<FlatList
					keyExtractor={(item)=> item.id}
						data={barraca}
						
						renderItem={({item}) => (
							
							<TouchableHighlight underlayColor="#CCCCCC" onPress={() => navigation.navigate('BarracaProducts', {indice:item.id})}>
								<View style={[styles.listaItem, {backgroundColor:item.bg}]}>
									<Image source={item.img} style={styles.listaImagem} />
									<View>
										<Text style={styles.listaTitle}>{item.title}</Text>
										<Text style={styles.listaDescription}>{item.description}</Text>
									</View>
								</View>
							</TouchableHighlight>
						)}
					/>
				</View>
			);
		};
	
		
	
		
	






const styles = StyleSheet.create({
	icone:{
		width:26,
		height:26
	},
	container:{
		flex:1
	},
	listaItem:{
		height:100,
		flex:1,
		flexDirection:'row',
		alignItems:'center'
	},
	listaImagem:{
		width:64,
		height:64,
		marginLeft:20,
		marginRight:20
	},
	listaTitle:{
		color:'#FFFFFF',
		fontSize:25,
		fontWeight:'bold'
	},
	listaDescription:{
		color:'#FFFFFF',
		fontSize:16
	}
});











