import React, { Component } from 'react';
import { TouchableHighlight,ImageBackground,Text,TextInput,Image,StyleSheet } from 'react-native';


export default class Home extends Component {
	static navigationOptions = {
		title:Home,
		header:null
	}

    render() {
		return (
			<ImageBackground source={require('../assets/images/logo.png')} style={styles.container}>
				<Text style={styles.login}></Text>
				<TextInput style={styles.input} placeholder='Digite seu email'></TextInput>
				<TextInput style={styles.input} placeholder='Digite sua senha' secureTextEntry={true}></TextInput>
				<TouchableHighlight style={styles.aButton}>
					<Text>Fazer login</Text>
				</TouchableHighlight>
			</ImageBackground>
			
		);
	}
}


const styles = StyleSheet.create({
	container:{
		flex:1,
		backgroundColor:'#CCCCCC',
		justifyContent:'center',
		alignItems:'center',
		resizeMode:'contain'
	},
	login:{
		fontSize:32,
		fontFamily:'Cochin' ,
		marginBottom:30,
		marginTop:180
	},
	input:{
		width:'90%',
		height:40,
		backgroundColor:'rgba(255, 255, 255,0.7)',
		color:'#FFFFFF',
		fontSize:14,
		marginBottom:10
		
	},
	aButton:{
		width:'90%',
		height:50,
		backgroundColor:'transparent',
		borderRadius:5,
		borderWidth:2,
		borderColor:'#FFFFFF',
		justifyContent:'center',
		alignItems:'center'
	}
})