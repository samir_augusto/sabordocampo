import React, { Component, useState } from 'react';
import { View, Text, Image, StyleSheet, FlatList , TouchableHighlight} from 'react-native';
import {useNavigation, useRoute } from "@react-navigation/native";

export default function BarracaProducts() {
	
	const navigation = useNavigation();
	const route = useRoute();
	const indice = route.params.indice;
	
	
	const [barraca, SetBarraca] = useState([
				{
							id:1, 
							name:'Alface', 
							img:require('../assets/images/produto/joao/alface.png')
						},
						{
							id:2, 
							name:'Banana', 
							img:require('../assets/images/produto/joao/banana.png')
						},
						{id:3, name:'Cebolinha', img:require('../assets/images/produto/joao/cebolinha.png')},
						{id:4, name:'Cenoura', img:require('../assets/images/produto/joao/cenoura.png')},
						{id:5, name:'Quejo de Cabra', img:require('../assets/images/produto/silva/farinha.png')},
						{id:6, name:'Fecula de Mandioca', img:require('../assets/images/produto/silva/fecula.png')},
						{id:7, name:'Mandioca', img:require('../assets/images/produto/silva/mandioca.png')},
						{id:8, name:'Doce de Banana', img:require('../assets/images/produto/maria/doceDeBanana.png')},
						{id:9, name:'DocedeLeite', img:require('../assets/images/produto/maria/doceDeLeite.png')},
						{id:10, name:'Queijo Fresco', img:require('../assets/images/produto/maria/queijo.png')},
						{id:11, name:'Jaca', img:require('../assets/images/produto/jose/jaca.png')},
						{id:12, name:'Limao', img:require('../assets/images/produto/jose/limao.png')},
						{id:13, name:'Mandioca', img:require('../assets/images/produto/jose/mandioca.png')}
					
					
				
			]);
					
			return (
				<View style={styles.container}>
				<FlatList
					keyExtractor={(item)=> item.id}
					data={barraca}
					
					renderItem={({item}) => (
						<View style={styles.productItem}>
						<Image resizeMode="contain" source={item.img} style={styles.productImagem} />
						<Text style={styles.productName}>{item.name}</Text>
					</View>
					)}
				/>
			</View>
								
			);
}
	

const styles = StyleSheet.create({
	icone:{
		width:26,
		height:26
	},
	container:{
		flex:1,
		backgroundColor:'#CCCCCC'
	},
	productItem:{
		height:100,
		backgroundColor:'#FFFFFF',
		margin:10,
		borderRadius:5,
		padding:10,
		flex:1,
		flexDirection:'row',
		alignItems:'center'
	},
	productImagem:{
		width:150,
		height:80
	},
	productName:{
		fontSize:16
	}
});

