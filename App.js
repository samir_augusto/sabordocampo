import React, { Component } from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import 'react-native-gesture-handler';
import { createStackNavigator } from '@react-navigation/stack';



import Home from './src/Home';
import Contato from './src/Contato';
import Sobre from './src/Sobre';
import Carrinho from './src/Carrinho';
import Barracas from './src/Barracas';
import BarracaList from './src/BarracaList';
import BarracaProducts from './src/BarracaProducts';


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
function Navigation(){
	return(
		<Stack.Navigator initialRouteName='BarracaList' >
				 <Stack.Screen name='BarracaList' component={BarracaList} />
				 <Stack.Screen name='BarracaProducts' component={BarracaProducts} />
			</Stack.Navigator>
		)
	
}



function MyTabs() {
	return (
	<Tab.Navigator initialRouteName='Home'>
				<Tab.Screen name="Home" component={Home} />
				<Tab.Screen name="Barracas" component={Navigation} />
				<Tab.Screen name="Carrinho" component={Carrinho} />
				<Tab.Screen name="Contato" component={Contato} />
				<Tab.Screen name="Sobre" component={Sobre} />
			</Tab.Navigator>
  )
}


export default function App(){
	return(
		<NavigationContainer>
			<MyTabs/>
		</NavigationContainer>
	)
	
	
}